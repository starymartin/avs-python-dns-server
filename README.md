# AVS Python DNS server #

Tento README súbor popisuje základné informácie semestrálnej práce.

### O čom je tento repozitár? ###

* DNS server v programovacom jazyku Python 3
* Funkcionalita  DNS servera je riešená použitím soketov
* Celá aplikácia je obalená REST API, ktorá je vytvorená pomocou balíčka Flask 
    * Repozitár obsahuje aj export z programu Postman v súbore
        * ```AvS - Python DNS Server.postman_collection [20210514].json```
* DNS server aktuálne podporuje 5 typy DNS záznamov podľa [RFC 1035](https://datatracker.ietf.org/doc/html/rfc1035)
    * SOA
    * NS
    * A
    * AAAA
    * CNAME

### Ako spustiť aplikáciu? ###

* Na inštaláciu potrebných závislostí je potrebné použiť nasledovný príkaz
    ```
    pip3 install -r requirements.txt
    ```
    
* Pre spustenie aplikácie je potrebné použiť nasledovné príkazy
    ```
    chmod a+x main.py
    sudo python3 main.py
    ```

### Dokumentácia endpointov ###

* Na dokumentáciu je použitý nástroj Swagger
* Dokumentácia je po spustení aplikácie dostupná na adrese
    [\<doména\>:\<port\>/api/doc](http://localhost:4000/api/doc/)
* Zoznam endpointov
    * ADMIN
        * POST: /api​/admin​/login Login
        * POST: /api​/admin​/logout Logout
        * GET: /api​/admin​/returnUserData Login
    * DNS
        * PUT: /api​/dns​/add-record​/{recordType}​/{zoneDomainName} Add DNS record
        * PUT: /api​/dns​/add-zone Add DNS zone
        * DELETE: /api​/dns​/delete-record​/{recordType}​/{domainName} Delete DNS record
        * DELETE: /api​/dns​/delete-zone​/{zoneDomainName} Delete DNS zone
        * GET: /api​/dns​/get-record​/{recordType}​/{domainName} Get DNS record
        * GET: /api​/dns​/get-zone​/{zoneDomainName} Get DNS zone
        * GET: /api​/dns​/get-zones Get all DNS zones
        * PATCH: ​/api​/dns​/start Start DNS server
        * PATCH: /api​/dns​/stop Stop DNS server

### Doplnkové funkcionality ###

#### Pridanie zónovy ####

* Pridanie zóny do bežiacej aplikácie
    * Toto je možné urobiť cez REST api rozhranie, cez endpoint ```/api​/dns​/add-zone```
    * Takéto pridanie sa neukladá permanentne do zónových súborov
* Pridanie zóny cez zónový súbor
    * Pre každú zónu existuje v adresári ```/sources/seeds/dnsZones``` jeden JSON súbor
    * Po pridaní JSON súboru je potrebný reštart aplikácie
    * Toto riešenie je permanentné

#### Pridanie používateľa ####
* Pridanie používateľa je možné len v JSON súbore ```/sources/seeds/users/user_seed.json``` 
* Pre získanie dát, ktoré je potrebné pridať do seeding súboru, je potrebné zavolať endpoint ```/api/admin/returnUserData```
* Po reštarte aplikácie je možné používať nového používateľa

# 🧟💻  #
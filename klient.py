import socket
import struct
import ipaddress

# RFC 1035, section 4.1.1 and 4.1.2; also sections 3.2.2 and 3.2.4
# Nice explanation: https://www2.cs.duke.edu/courses/fall16/compsci356/DNS/DNS-primer.pdf

DNS_ADDRESS = "0.0.0.0"
DNS_PORT = 53
BIND_PORT = 50000

# Creation of header
transaction_id = socket.htons(0x1234)
flags = socket.htons(0x0100)  # Standard query with recursion desired
questions = socket.htons(0x0002)
answer = 0x0000
authority = 0x0000
additional = 0x0000

# small 'h' is format character for short - 2 bytes
header = struct.pack('hhhhhh', transaction_id, flags, questions, answer, authority, additional)


question = "www.uniza.sk"
labels = question.split('.')

# Initialize body of query in byte literal
body_of_query = b''
# pocet_znakov znaky pocet_znakov znaky ... 0x00

# For each label in query
for label in labels:
    # Add length of label in binary to body
    body_of_query += bytes([len(label)])

    # For each char in label
    for c in label:
        # Add binary representation in ASCII
        body_of_query += bytes([ord(c)])

# Add zero to the end of body
body_of_query += bytes([0x00])


# otazka 2
question = "www.tuke.sk"
labels = question.split('.')

# Initialize body of query in byte literal
body_of_query2 = b''
# pocet_znakov znaky pocet_znakov znaky ... 0x00

# For each label in query
for label in labels:
    # Add length of label in binary to body
    body_of_query2 += bytes([len(label)])

    # For each char in label
    for c in label:
        # Add binary representation in ASCII
        body_of_query2 += bytes([ord(c)])

# Add zero to the end of body
body_of_query2 += bytes([0x00])


# End of query
query_type = socket.htons(0x0001)  # A
query_class = socket.htons(0x0001)  # IN

end_of_query = struct.pack('hh', query_type, query_class)
#print(header)

# Create socket, send and close socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# try to bind to a port
try:
    sock.bind(('', BIND_PORT))
except socket.error as e:
    print("Error bind to port")
    exit(1)

# send request
sock.sendto(header + body_of_query + end_of_query + body_of_query2 + end_of_query, (DNS_ADDRESS, DNS_PORT))


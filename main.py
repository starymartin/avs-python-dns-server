# Loading from '.env' file to enviroment variables
from dotenv import load_dotenv
load_dotenv()

# Flask app
from flask import Flask
app = Flask(__name__)

import configs.swagger_config as SWAGGER_CONFIG
import configs.app_config as APP_CONFIG
import configs.logger_config as LOGS_CONFIG
APP_CONFIG.APP_ROOT = app.root_path

import sources.routes.admin.admin_route as ADMIN_ROUTE
import sources.routes.dns.dns_route as DNS_ROUTE

logInfo = {"label": "[MAIN]"}

# Swagger json
@app.route(SWAGGER_CONFIG.SWAGGER_URL + ".json")
def swaggerJson():
    return SWAGGER_CONFIG.SWAGGER_JSON(app)

# Swagger UI
app.register_blueprint(SWAGGER_CONFIG.SWAGGER_UI_FLASK_BLUEPRINT)

# APP routes
app.register_blueprint(ADMIN_ROUTE.admin, url_prefix=APP_CONFIG.APP_ROUTE_PREFIX + "/admin")
app.register_blueprint(DNS_ROUTE.dns, url_prefix=APP_CONFIG.APP_ROUTE_PREFIX + "/dns")

if __name__ == '__main__':
    try:
        app.run(APP_CONFIG.APP_HOST, APP_CONFIG.APP_PORT, False)
    except Exception as exc:
        LOGS_CONFIG.logError("Start FLASK API failed", logInfo, 6, exc)
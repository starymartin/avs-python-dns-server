import json
import struct
import os
import configs.app_config as APP_CONFIG
import configs.logger_config as LOGS_CONFIG
import sources.models.dns.dnsMessage_model as DNS_MESSAGE_MODEL
import re
import socket
import copy

logInfo = {"label": "[DNS SERVER MODEL]"}

class DNS_Server:
    def __init__(self):
        self.zones = {}


    """ PUBLIC CLASS METHOD, params: {str:domain}
            Function used for finding best-match zone domain name
        return {str} """ 
    def findZone(self, domain):
        foundedZone = None
        for zone in self.zones.keys():
            if(zone == domain):
                return zone
            if(re.search("\." + zone +"$", domain)):
                print(zone)
                if(foundedZone == None):
                    foundedZone = zone
                else:
                    if(len(foundedZone) < len(zone)):
                        foundedZone = zone
        return foundedZone


    """ PRIVATE CLASS METHOD, params: {dict:questionZone}, {str:questionDomain}, {OPT:bool:dnsServer}
            Function used for finding CNAMES for domain from parameter within zone specified in parameter 
        return {dict} """ 
    def __findCNAME(self, questionZone, questionDomain, dnsServer = True):
        result = {
            "cnames": list(),
            "ipv4Address": list(),
            "ipv6Address": list()
        }
        lastFoundedDomainName = questionDomain
        foundedCNAME = questionZone.get("CNAME_RECORDS")
        if(foundedCNAME != None):
            foundedCNAME = foundedCNAME.get(questionDomain)  
        while(foundedCNAME != None):
            if(dnsServer == True):
                result["cnames"].append(DNS_MESSAGE_MODEL.DNS_RR(foundedCNAME["nameBytes"], 5, 1, foundedCNAME["ttl"], len(foundedCNAME["valueBytes"]), foundedCNAME["valueBytes"], foundedCNAME["value"]))
            else:
                result["cnames"].append({"name": lastFoundedDomainName, "TTL": foundedCNAME["ttl"], "data": foundedCNAME["value"], "type": 5})
            lastFoundedDomainName = foundedCNAME["value"]   
            foundedCNAME = questionZone["CNAME_RECORDS"].get(foundedCNAME["value"])
        if(len(result["cnames"])):
            lastCname = result["cnames"][-1]
            if(dnsServer == True):
                foundedIPv4Address = questionZone["A_RECORDS"].get(lastCname.domainName)
                foundedIPv6Address = questionZone["AAAA_RECORDS"].get(lastCname.domainName)
            else:
                foundedIPv4Address = questionZone["A_RECORDS"].get(lastCname["data"])
                foundedIPv6Address = questionZone["AAAA_RECORDS"].get(lastCname["data"])
            if(foundedIPv4Address != None):
                if(dnsServer == True):
                    result["ipv4Address"].append(DNS_MESSAGE_MODEL.DNS_RR(foundedIPv4Address["nameBytes"], 1, 1, foundedIPv4Address["ttl"], len(foundedIPv4Address["valueBytes"]), foundedIPv4Address["valueBytes"], foundedIPv4Address["value"]))
                else:
                    result["ipv4Address"].append({"name": lastCname["data"], "TTL": foundedIPv4Address["ttl"], "data": foundedIPv4Address["value"], "type": 1})
            if(foundedIPv6Address != None):
                if(dnsServer == True):
                    result["ipv6Address"].append(DNS_MESSAGE_MODEL.DNS_RR(foundedIPv6Address["nameBytes"], 28, 1, foundedIPv6Address["ttl"], len(foundedIPv6Address["valueBytes"]), foundedIPv6Address["valueBytes"], foundedIPv6Address["value"]))
                else:
                    result["ipv6Address"].append({"name": lastCname["data"], "TTL": foundedIPv6Address["ttl"], "data": foundedIPv6Address["value"], "type": 28})
        return result


    """ PRIVATE CLASS METHOD, params: {str:questionZone}, {OPT:bool:dnsServer}
            Function used for creating default DNS SOA answer  
        return {dict or class<DNS_RR>} """
    def __defaultSOA(self, questionZone, dnsServer = True):
        zone = self.zones[questionZone]
        zoneSOA = zone["SOA"]
        if(dnsServer == True):
            soaAnswerRDATA = zoneSOA["MNAME_Bytes"] + zoneSOA["RNAME_Bytes"] + struct.pack("!LLLLL", zoneSOA["SERIAL"], zoneSOA["REFRESH"], zoneSOA["RETRY"], zoneSOA["EXPIRE"], zoneSOA["MINIMUM"]) 
            return DNS_MESSAGE_MODEL.DNS_RR(zone["zoneBytes"], 6, 1, zone["ttl"], len(soaAnswerRDATA), soaAnswerRDATA)
        else:
            data = zoneSOA["MNAME"] + ". " + zoneSOA["RNAME"] + ". " + str(zoneSOA["REFRESH"]) + " " + str(zoneSOA["RETRY"]) + " " + str(zoneSOA["EXPIRE"]) + " " + str(zoneSOA["MINIMUM"])
            return {"name": questionZone, "TTL": zone["ttl"], "data": data, "type": 6}


    #(THE BEGINING OF)*********************************************** NETWORK DNS SERVER ***********************************************#
    """ PRIVATE CLASS METHOD, params: {dict:credentials}
            Function used for creating DNS A type answer
        return {dict} """
    def __answerA(self, credentials):
        questionZone, questionType, questionDomain, questionDomainBytes, response, zone, cnameResult = credentials
        if(len(cnameResult["cnames"]) == 0):
            a_record = zone.get("A_RECORDS")
            if(a_record == None):
                response["authorityRRs"].append(self.__defaultSOA(questionZone))
                return response
            a_record = a_record.get(questionDomain) 
            if(a_record != None):
                response["answerRRs"].append(DNS_MESSAGE_MODEL.DNS_RR(questionDomainBytes, questionType, 1, a_record["ttl"], len(a_record["valueBytes"]), a_record["valueBytes"]))
            else:
                response["authorityRRs"].append(self.__defaultSOA(questionZone))
        else:
            if(len(cnameResult["ipv4Address"])):
                response["answerRRs"].extend(cnameResult["ipv4Address"])
            else:
                response["authorityRRs"].append(self.__defaultSOA(questionZone))
        return response
    

    """ PRIVATE CLASS METHOD, params: {dict:credentials}
            Function used for creating DNS NS type answer
        return {dict} """
    def __answerNS(self, credentials):
        questionZone, questionType, questionDomain, questionDomainBytes, response, zone, cnameResult = credentials
        if(questionDomain == questionZone and zone.get("NS_RECORDS") != None):
            for nsRecord in zone["NS_RECORDS"]:
                response["answerRRs"].append(DNS_MESSAGE_MODEL.DNS_RR(questionDomainBytes, questionType, 1, zone["ttl"], len(zone["NS_RECORDS"][nsRecord]["hostBytes"]), zone["NS_RECORDS"][nsRecord]["hostBytes"]))
        else:
            response["authorityRRs"].append(self.__defaultSOA(questionZone))
        return response


    """ PRIVATE CLASS METHOD, params: {dict:credentials}
            Function used for creating DNS CNAME type answer
        return {dict} """
    def __answerCNAME(self, credentials):
        questionZone, questionType, questionDomain, questionDomainBytes, response, zone, cnameResult = credentials
        if(len(cnameResult["cnames"]) == 0):
            response["authorityRRs"].append(self.__defaultSOA(questionZone))
        return response


    """ PRIVATE CLASS METHOD, params: {dict:credentials}
            Function used for creating DNS SOA type answer
        return {dict} """
    def __answerSOA(self, credentials):
        questionZone, questionType, questionDomain, questionDomainBytes, response, zone, cnameResult = credentials
        if(questionZone == questionDomain):
            response["answerRRs"].append(self.__defaultSOA(questionZone))
        else:
            response["authorityRRs"].append(self.__defaultSOA(questionZone))
        return response


    """ PRIVATE CLASS METHOD, params: {dict:credentials}
            Function used for creating DNS AAAA type answer
        return {dict} """
    def __answerAAAA(self, credentials):
        questionZone, questionType, questionDomain, questionDomainBytes, response, zone, cnameResult = credentials
        if(len(cnameResult["cnames"]) == 0):
            aaaa_record = zone.get("AAAA_RECORDS")
            if(aaaa_record == None):
                response["authorityRRs"].append(self.__defaultSOA(questionZone))
                return response
            aaaa_record = aaaa_record.get(questionDomain) 
            if(aaaa_record != None):
                response["answerRRs"].append(DNS_MESSAGE_MODEL.DNS_RR(questionDomainBytes, questionType, 1, aaaa_record["ttl"], len(aaaa_record["valueBytes"]), aaaa_record["valueBytes"]))
            else:
                response["authorityRRs"].append(self.__defaultSOA(questionZone))
        else:
            if(len(cnameResult["ipv6Address"])):
                response["answerRRs"].extend(cnameResult["ipv6Address"])
            else:
                response["authorityRRs"].append(self.__defaultSOA(questionZone))
        return response


    """ PUBLIC CLASS METHOD, params: {dict:credentials}
            Function used as interface to create DNS answer for DNS query type specified in credentials
        return {dict} """
    def searchByCredetials(self, credentials):
        response = {
            "answerRRs": list(),
            "authorityRRs": list(),
            "additionalRRs": list()
        }
        questionZone, questionType, questionDomain, questionDomainBytes = credentials
        if(questionZone == None):
            return response
        zone = self.zones[questionZone]
        cnameResult = self.__findCNAME(zone, questionDomain)
        response["answerRRs"] = cnameResult["cnames"]
        switchLocal = {
            1: self.__answerA, 
            2: self.__answerNS, 
            5: self.__answerCNAME,
            6: self.__answerSOA,
            28: self.__answerAAAA       
        }
        return switchLocal.get(questionType, "Unsupported question type")((questionZone, questionType, questionDomain, questionDomainBytes, response, zone, cnameResult))
    #(THE END OF)*********************************************** NETWORK DNS SERVER ***********************************************#


    """ PUBLIC CLASS METHOD, params: {OPT:str:zoneString}, {OPT:dict:zone}
            Function used to return copy of zone, which attributes are cleared from bytes data, which can't be serialized during jsonify
        return {dict} """
    def getClearedZone(self, zoneString = "", zone = None):
        if(zone == None):
            zone = self.zones.get(zoneString, None)
            if(zone == None):
                return zone
        zoneCopy = zone.copy()
        zoneCopy.pop("zoneBytes", None)
        zoneCopy["SOA"].pop("MNAME_Bytes", None)
        zoneCopy["SOA"].pop("RNAME_Bytes", None)
        if(zoneCopy.get("NS_RECORDS") != None):
            for ns_record in zoneCopy["NS_RECORDS"].keys():
                zoneCopy["NS_RECORDS"][ns_record].pop("hostBytes", None)
        if(zoneCopy.get("CNAME_RECORDS") != None):
            for cname_record in zoneCopy["CNAME_RECORDS"].keys():
                zoneCopy["CNAME_RECORDS"][cname_record].pop("nameBytes", None)
                zoneCopy["CNAME_RECORDS"][cname_record].pop("valueBytes", None)
        if(zoneCopy.get("A_RECORDS") != None):
            for a_record in zoneCopy["A_RECORDS"].keys():
                zoneCopy["A_RECORDS"][a_record].pop("nameBytes", None)
                zoneCopy["A_RECORDS"][a_record].pop("valueBytes", None)
        if(zoneCopy.get("AAAA_RECORDS") != None):
            for aaaa_record in zoneCopy["AAAA_RECORDS"].keys():
                zoneCopy["AAAA_RECORDS"][aaaa_record].pop("nameBytes", None)
                zoneCopy["AAAA_RECORDS"][aaaa_record].pop("valueBytes", None)
        return zoneCopy

    #(THE BEGINNING OF)*********************************************** DNS OVER HTTP SERVER ***********************************************#
    """ PRIVATE CLASS METHOD, params: {dict:credentials}
            Function used for creating DNS A type answer for DNS over HTTP
        return {dict} """
    def __answerA_HTTP(self, credentials):
        dnsRecord, dnsZone, response, cnameResult, questionZone = credentials
        if(len(cnameResult["cnames"]) == 0):
            a_record = dnsZone.get("A_RECORDS")
            if(a_record == None):
                response["Authority"].append(self.__defaultSOA(questionZone, False))
                return response
            a_record = a_record.get(dnsRecord) 
            if(a_record != None):
                response["Answer"].append({"name": dnsRecord, "TTL": a_record["ttl"], "data": a_record["value"], "type": 5})
            else:
                response["Authority"].append(self.__defaultSOA(questionZone, False))
        else:
            if(len(cnameResult["ipv4Address"])):
                response["Answer"].extend(cnameResult["ipv4Address"])
            else:
                response["Authority"].append(self.__defaultSOA(questionZone, False))
        return response


    """ PRIVATE CLASS METHOD, params: {dict:credentials}
            Function used for creating DNS NS type answer for DNS over HTTP
        return {dict} """
    def __answerNS_HTTP(self, credentials):
        dnsRecord, dnsZone, response, cnameResult, questionZone = credentials
        if(dnsRecord == questionZone and dnsZone.get("NS_RECORDS") != None):
            for nsRecord in dnsZone["NS_RECORDS"]:
                response["Answer"].append({"name": dnsRecord, "TTL": dnsZone["ttl"], "data": dnsZone["NS_RECORDS"][nsRecord]["host"], "type": 2})
        else:
            response["Authority"].append(self.__defaultSOA(questionZone, False))
        return response


    """ PRIVATE CLASS METHOD, params: {dict:credentials}
            Function used for creating DNS CNAME type answer for DNS over HTTP
        return {dict} """
    def __answerCNAME_HTTP(self, credentials):
        dnsRecord, dnsZone, response, cnameResult, questionZone = credentials
        if(len(cnameResult["cnames"]) == 0):
            response["Authority"].append(self.__defaultSOA(questionZone, False))
        return response


    """ PRIVATE CLASS METHOD, params: {dict:credentials}
            Function used for creating DNS SOA type answer for DNS over HTTP
        return {dict} """
    def __answerSOA_HTTP(self, credentials):
        dnsRecord, dnsZone, response, cnameResult, questionZone = credentials
        if(questionZone == dnsRecord):
            response["Answer"].append(self.__defaultSOA(questionZone, False))
        else:
            response["Authority"].append(self.__defaultSOA(questionZone, False))
        return response


    """ PRIVATE CLASS METHOD, params: {dict:credentials}
            Function used for creating DNS AAAA type answer for DNS over HTTP
        return {dict} """
    def __answerAAAA_HTTP(self, credentials):
        dnsRecord, dnsZone, response, cnameResult, questionZone = credentials
        if(len(cnameResult["cnames"]) == 0):
            aaaa_record = dnsZone.get("AAAA_RECORDS")
            if(aaaa_record == None):
                response["Authority"].append(self.__defaultSOA(questionZone, False))
                return response
            aaaa_record = aaaa_record.get(dnsRecord) 
            if(aaaa_record != None):
                response["Answer"].append({"name": dnsRecord, "TTL": aaaa_record["ttl"], "data": aaaa_record["value"], "type": 28})
            else:
                response["Authority"].append(self.__defaultSOA(questionZone, False))
        else:
            if(len(cnameResult["ipv6Address"])):
                response["Answer"].extend(cnameResult["ipv6Address"])
            else:
                response["Authority"].append(self.__defaultSOA(questionZone, False))
        return response


    """ PUBLIC CLASS METHOD, params: {dict:credentials}
            Function used as interface to create DNS over HTTP answer for DNS query type specified in credentials
        return {dict} """
    def searchByCredetialsHTTP(self, recordType, dnsRecord):
        response = {
            "Answer": list(),
            "Authority": list(),
            "Additional": list()
        }
        questionZone = self.findZone(dnsRecord)
        if(questionZone == None):
            return response
        zone = self.zones[questionZone]
        cnameResult = self.__findCNAME(zone, dnsRecord, False)
        response["Answer"] = cnameResult["cnames"]
        switchLocal = {
            "A": self.__answerA_HTTP, 
            "NS": self.__answerNS_HTTP, 
            "CNAME": self.__answerCNAME_HTTP,
            "SOA": self.__answerSOA_HTTP,
            "AAAA": self.__answerAAAA_HTTP       
        }
        return switchLocal.get(recordType, "Unsupported question type")((dnsRecord, zone, response, cnameResult, questionZone))
    #(THE END OF)*********************************************** DNS OVER HTTP SERVER ***********************************************#


    """ PUBLIC CLASS METHOD, params: {str:origin}, {int:ttl}, {dict:soa}
            Function used to add new zone
        return {dict} """
    def addZone(self, origin, ttl, soa):
        addedZoneToReturn = {
            "ttl": ttl,
            "SOA": {
                "MNAME": soa["mname"],
                "RNAME": soa["rname"],
                "SERIAL": int(soa["serial"]),
                "REFRESH": int(soa["refresh"]),
                "RETRY": int(soa["retry"]),
                "EXPIRE": int(soa["expire"]),
                "MINIMUM": int(soa["minimum"])
            }
        }
        addedZoneWithBytes = copy.deepcopy(addedZoneToReturn)
        addedZoneWithBytes["zoneBytes"] = DNS_MESSAGE_MODEL.formateDomainName(origin)
        addedZoneWithBytes["SOA"]["MNAME_Bytes"] = DNS_MESSAGE_MODEL.formateDomainName(soa["mname"])
        addedZoneWithBytes["SOA"]["RNAME_Bytes"] = DNS_MESSAGE_MODEL.formateDomainName(soa["rname"])
        self.zones[origin] = addedZoneWithBytes
        return addedZoneToReturn
    

    """ PUBLIC CLASS METHOD, params: {OPT:str:zonesFolder}
            Function used to seed zones from JSON files to this class
        return {dict} """
    def seedZones(self, zonesFolder = APP_CONFIG.APP_ROOT + "/sources/seeds/dnsZones"):
        try:
            zones = os.listdir(zonesFolder)
            for zone in zones:
                with open(zonesFolder + "/" + zone) as json_file:
                    zoneJson = json.load(json_file)
                    parsedZone = {}
                    parsedZone["ttl"] = int(zoneJson["ttl"])
                    parsedZone["zoneBytes"] = DNS_MESSAGE_MODEL.formateDomainName(zoneJson["origin"])
                    parsedZone["SOA"] = {
                        "MNAME": zoneJson["soa"]["mname"],
                        "RNAME": zoneJson["soa"]["rname"],
                        "MNAME_Bytes": DNS_MESSAGE_MODEL.formateDomainName(zoneJson["soa"]["mname"]),
                        "RNAME_Bytes": DNS_MESSAGE_MODEL.formateDomainName(zoneJson["soa"]["rname"]),
                        "SERIAL": int(zoneJson["soa"]["serial"]),
                        "REFRESH": int(zoneJson["soa"]["refresh"]),
                        "RETRY": int(zoneJson["soa"]["retry"]),
                        "EXPIRE": int(zoneJson["soa"]["expire"]),
                        "MINIMUM": int(zoneJson["soa"]["minimum"])
                    }
                    parsedZone["NS_RECORDS"] = {}
                    for ns in zoneJson["ns_records"]:
                        parsedZone["NS_RECORDS"][ns["host"]] = {
                            "host": ns["host"],
                            "hostBytes": DNS_MESSAGE_MODEL.formateDomainName(ns["host"])
                        }
                    parsedZone["CNAME_RECORDS"] = {}
                    for cname in zoneJson["cname_records"]:
                        parsedZone["CNAME_RECORDS"][cname["name"]] = {
                            "nameBytes": DNS_MESSAGE_MODEL.formateDomainName(cname["name"]),
                            "ttl": cname["ttl"],
                            "value": cname["value"],
                            "valueBytes": DNS_MESSAGE_MODEL.formateDomainName(cname["value"]),
                        }
                    parsedZone["A_RECORDS"] = {}
                    for a_record in zoneJson["a_records"]:
                        parsedZone["A_RECORDS"][a_record["name"]] = {
                            "nameBytes": DNS_MESSAGE_MODEL.formateDomainName(a_record["name"]),
                            "ttl": a_record["ttl"],
                            "value": a_record["value"],
                            "valueBytes": socket.inet_pton(socket.AF_INET, a_record["value"])
                        }
                    parsedZone["AAAA_RECORDS"] = {}
                    for aaaa_record in zoneJson["aaaa_records"]:
                        parsedZone["AAAA_RECORDS"][aaaa_record["name"]] = {
                            "nameBytes": DNS_MESSAGE_MODEL.formateDomainName(aaaa_record["name"]),
                            "ttl": aaaa_record["ttl"],
                            "value": aaaa_record["value"],
                            "valueBytes": socket.inet_pton(socket.AF_INET6, aaaa_record["value"])
                        }
                    self.zones[zoneJson["origin"]] = parsedZone
        except Exception as exc:
            LOGS_CONFIG.logError("Unable to seed DNS zones", logInfo, -1, exc)
import struct
import ctypes
import configs.app_config as APP_CONFIG

logInfo = {"label": "[DNS MESSAGE MODEL]"}

c_uint8 = ctypes.c_uint8
HEADER_LENGTH = 12

class Flags1_bits( ctypes.LittleEndianStructure ):
    _fields_ = [
        ("RD",      ctypes.c_uint8, 1 ), # bit 7
        ("TC",      ctypes.c_uint8, 1 ), # bit 6
        ("AA",      ctypes.c_uint8, 1 ), # bit 5
        ("OPCODE",  ctypes.c_uint8, 4 ), # bit 1-4
        ("QR",      ctypes.c_uint8, 1 ), # bit 0
    ]
 
class Flags1( ctypes.Union ):
    _anonymous_ = ("bit",)
    _fields_ = [
        ("bit",    Flags1_bits ),
        ("asByte", c_uint8    )
    ]

class Flags2_bits( ctypes.LittleEndianStructure ):
    _fields_ = [
        ("RCODE",   ctypes.c_uint8, 4 ), # bit 4-7
        ("Z",       ctypes.c_uint8, 3 ), # bit 1-3
        ("RA",      ctypes.c_uint8, 1 ), # bit 0
    ]
 
class Flags2( ctypes.Union ):
    _anonymous_ = ("bit",)
    _fields_ = [
        ("bit",    Flags2_bits ),
        ("asByte", c_uint8    )
    ]

class DNS_Header:
    def __init__(self, paInput):
        paID, paFlags1, paFlags2, paQDcount, paANcount, paNScount, paARcount = paInput
        self.flags1 = Flags1()
        self.flags2 = Flags2()
        self.flags1.asByte = paFlags1
        self.flags2.asByte = paFlags2
        self.ID = paID              # 2B - Identifikator query-response spojenia
        #self.flags1.QR             # 1b - Query = 0, Response = 1
        #self.flags1.OPCODE         # 4b - 0 = standard query (QUERY), 1 = inverse query (IQUERY), 2 = server status request (STATUS), 3-15 = reserved for future use
        #self.flags1.AA             # 1b - Autoritativna odpoved
        #self.flags1.TC             # 1b - TrucCation = sprava bola dlhsia ako 512B a bude poslana cez TCP
        #self.flags1.RD             # 1b - Recursive query
        #self.flags2.RA             # 1b - Recursion available
        #self.flags2.Z              # 3b - Reserved for future
        #self.flags2.RCODE          # 4b - Response code => 0 = No error, 1 = Format error, 2 = Server failure, 3 = Name error, 4 = Not implemented, 5 = Refused, 6-15 = Reserved for future
        self.QDCOUNT = paQDcount    # 2B - Pocet queries
        self.ANCOUNT = paANcount    # 2B - Pocet responses
        self.NSCOUNT = paNScount    # 2B - Pocet v authority records section 
        self.ARCOUNT = paARcount    # 2B - Pocet v additional records section


    """ PUBLIC CLASS METHOD, params: {bytearray:paByteArray}
            Function used for parsing received byte-array to DNS header and calling constructor of this class
        return {class<DNS_Header>} """
    @classmethod
    def parseHeader(cls, paByteArray):
        return cls(struct.unpack("!HBBHHHH", paByteArray[:HEADER_LENGTH]))


    """ PUBLIC CLASS METHOD, params: None
            Function used for packing this class data to DNS header byte-array form
        return {bytes} """
    def getHeader(self):
        header = struct.pack("!HBBHHHH", 
            self.ID,
            self.flags1.asByte,
            self.flags2.asByte,
            self.QDCOUNT, 
            self.ANCOUNT, 
            self.NSCOUNT, 
            self.ARCOUNT
        )    
        return header

class DNS_Query:
    def __init__(self, paInput):
        paDomainName, paQName, paQType, paQCLass = paInput
        self.domainName = paDomainName # list() pozostavajuci z labels
        self.QNAME = paQName           # -- - DYNAMICKA VELKOST - Domain name, pricom namiesto bodiek sa nachadza pocet znakov (bajtov) nasledujuceho label-u, pricom za domain name sa nachadza znak '\0' 
        self.QTYPE = paQType           # 2B - Record type => 1 = A, 2 = NS, 3 = MD, 4 = MF, 5 = CNAME, 6 = SOA, 7 = MB, 8 = MG, 9 = MR, 10 = NULL, 11 = WKS, 12 = PTR, 13 = HINFO, 14 = MINFO, 15 = MX, 16 = TXT, 28 = AAAA
        self.QCLASS = paQCLass         # 2B - Query class => 1 = IN (Internet)


    """ PUBLIC CLASS METHOD, params: None
            Function used for packing this class data to DNS question byte-array form
        return {bytes} """
    def getQuery(self):
        query = self.QNAME + struct.pack("!HH", 
            self.QTYPE,
            self.QCLASS
        )    
        return query

class DNS_Queries:
    def __init__(self, paQueries):
        self.queries = paQueries        #list of DNS_Query classes
    

    """ PUBLIC CLASS METHOD, params: {bytearray:paByteArray, int:paAmountOfQueries}
            Function used for parsing received byte-array to multiple DNS queries and calling constructor of this class
        return {class<DNS_Header>} """
    @classmethod
    def parseQueries(cls, paByteArray, paAmountOfQueries):
        index = HEADER_LENGTH + 1
        labelLength = paByteArray[HEADER_LENGTH] 
        queries = list()
        for i in range(1, paAmountOfQueries+1): #1-paAmountOfQueries, inac by bolo 0-paAmountOfQueries
            qDomainName = list()
            while labelLength != 0:
                qDomainName.append(paByteArray[index:index + labelLength].decode())
                index += labelLength + 1
                labelLength = paByteArray[index-1]
            qName = paByteArray[HEADER_LENGTH:index]
            qType, qClass = struct.unpack("!HH", paByteArray[index:index+4])
            queries.append(DNS_Query((qDomainName, qName, qType, qClass)))
            if i < paAmountOfQueries:
                labelLength = paByteArray[index]
        return cls(queries)
    

    """ PUBLIC CLASS METHOD, params: None
            Function used for packing this class data to multiple DNS questions byte-array form
        return {bytes} """
    def getQueries(self):
        if(len(self.queries) == 0):
            return bytearray()
        queries = self.queries[0].getQuery()
        for i in range(1, len(self.queries)):
            queries += self.queries[i].getQuery()
        return queries

class DNS_RR:
    def __init__(self, paName, paType, paClass, paTTL, paRDLength, paRData, domainName = ""):
        self.domainName = domainName
        self.NAME = paName
        self.TYPE = paType
        self.CLASS = paClass
        self.TTL = paTTL
        self.RDLENGTH = paRDLength
        self.RDATA = paRData


    """ PUBLIC CLASS METHOD, params: None
            Function used for packing this class data to DNS record byte-array form
        return {bytes} """
    def getRR(self):
        RR = self.NAME + struct.pack("!HHLH",
            self.TYPE,
            self.CLASS,
            self.TTL,
            self.RDLENGTH
        ) + self.RDATA
        return RR

class DNS_RRs:
    def __init__(self):
        self.RRs = list()        #list of DNS_RR classes
    
    """ PUBLIC CLASS METHOD, params: None
            Function used for packing this class data to multiple DNS records byte-array form
        return {bytes} """
    def getRRs(self):
        if(len(self.RRs) == 0):
            return bytearray()
        RRs = self.RRs[0].getRR()
        for i in range(1, len(self.RRs)):
            RRs += self.RRs[i].getRR()
        return RRs

""" PUBLIC, params: {str:domainName}
        Function used for formatting from dot-domain name format to DNS domain name format
    return {bytes} """
def formateDomainName(domainName):
    formatted = list("." + domainName + "\0")
    splitted = domainName.split(".")
    splittedLen = len(splitted)
    index, indexDot = (0,0)
    for char in formatted:
        if(char == '.'):
            formatted[index] = chr(len(splitted[indexDot]))
            indexDot += 1
        index += 1
    return "".join(formatted).encode()
import json
import jwt
import uuid
import bcrypt
from datetime import datetime, timedelta
import configs.app_config as APP_CONFIG
import configs.logger_config as LOGS_CONFIG

logInfo = {"label": "[USER MODEL]"}

class UserModel:
    def __init__(self, username, password, uuid = None, hashedPassword = True):
        if(uuid == None):
            self.uuid = uuid.uuid4()
        else:
            self.uuid = uuid
        self.username = username
        if(hashedPassword):
            self.passwordHash = password.encode('ascii')
        else:
            self.passwordHash = bcrypt.hashpw(password.encode('ascii'), bcrypt.gensalt())
        self.tokens = list()

    """ PUBLIC CLASS METHOD, params: {str:username}, {str:password}
            Function used for loggin user using plain-text password
        return {dict} """
    def login(self, username, password):
        if(self.username != username):
            return {"successful": False, "error": "Admin is not authorized. Access denied"}

        if(bcrypt.checkpw(password.encode('ascii'), self.passwordHash)):
            for i in range(len(self.tokens)):
                tokenDecoded = jwt.decode(self.tokens[i], APP_CONFIG.JWT_SECRET, algorithms=["HS256"])
                if(datetime.now() > datetime.strptime(tokenDecoded["expiration"], "%Y-%m-%d %H:%M:%S.%f")):
                    del self.tokens[i]

            if(APP_CONFIG.USER_MAX_TOKENS != 0 and len(self.tokens) >= APP_CONFIG.USER_MAX_TOKENS):
                return {"successful": False, "error": "User is using all available tokens"}

            jwt_payload = {
                "uuid": self.uuid,
                "expiration": str(datetime.now() + timedelta(minutes = APP_CONFIG.USER_TOKEN_EXPIRATION))
            }
            token = jwt.encode(jwt_payload, APP_CONFIG.JWT_SECRET, algorithm="HS256")
            self.tokens.append(token)
            return {"successful": True, "token": token}
            
        return {"successful": False, "error": "Admin is not authorized. Access denied"}

class Users:
    def __init__(self):
        self.users = {}
        self.count = 0

    """ PUBLIC CLASS METHOD, params: {OPT:str:seedFile}
            Function used to seed users from JSON file to this class
        return None """
    def seedUsers(self, seedFile = APP_CONFIG.APP_ROOT + "/sources/seeds/users/users_seed.json"):
        try:
            with open(seedFile) as json_file:
                usersJson = json.load(json_file)
                for user in usersJson:
                    self.users[user["uuid"]] = UserModel(user["username"], user["passwordHash"], user["uuid"])
                self.count = len(self.users)
        except Exception as exc:
            LOGS_CONFIG.logError("Unable to seed users", logInfo, -1, exc)
    
    """ PUBLIC CLASS METHOD, params: {str:username}
            Function used for returning user by username
        return {class<UserModel>} """
    def getUserByUsername(self, username):
        for uuid in self.users:
            user = self.users[uuid]
            if(user.username == username):
                return user
        return None
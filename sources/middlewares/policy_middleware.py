import jwt
from flask import jsonify, request
from datetime import datetime
import sources.controllers.admin.admin_controller as ADMIN_CONTROLLER
import configs.app_config as APP_CONFIG
import configs.logger_config as LOGS_CONFIG

logInfo = {"label": "[PRIVACY MIDDLEWARE]"}

""" MIDDLEWARE
    EMPTY BODY 
    REQUIRED HEADERS: {authorization, format:'Basic {base64}'} """
def auth():
    try:
        token = request.headers.get('authorization')
        if(token == None):
            LOGS_CONFIG.logError("Not authorized for operation. Access denied", logInfo, 401)
            return {"success": False, "error": "Not authorized for operation. Access denied", "errorCode": 401}
        token = token.replace("Bearer ", "")
        tokenDecoded = jwt.decode(token, APP_CONFIG.JWT_SECRET, algorithms=["HS256"])
        users = ADMIN_CONTROLLER.USERS_MODEL
        try:
            user = users.users[tokenDecoded["uuid"]]
        except:
            LOGS_CONFIG.logError("Not authorized for operation. Access denied", logInfo, 401)
            return {"success": False, "error": "Not authorized for operation. Access denied", "errorCode": 401}
        try:
            tokenIndex = user.tokens.index(token)
        except:
            LOGS_CONFIG.logError("Not authorized for operation. Access denied", logInfo, 401)
            return {"success": False, "error": "Not authorized for operation. Access denied", "errorCode": 401}
        if(datetime.now() > datetime.strptime(tokenDecoded["expiration"], "%Y-%m-%d %H:%M:%S.%f")):
            del user.tokens[tokenIndex]
            LOGS_CONFIG.logError("Not authorized for operation. Access denied", logInfo, 401)
            return {"success": False, "error": "Not authorized for operation. Access denied", "errorCode": 401}
        return {"success": True, "user": user, "tokenIndex": tokenIndex}
    except Exception as exc:
        LOGS_CONFIG.logError("Admin authorization has failed. Access denied", logInfo, error=exc)
        return {"success": False, "error": "Admin authorization has failed. Access denied", "errorCode": 401}

"""
Solution with decorartor | issue with sending data to next()
from functools import wraps
def auth_decorator():
    def _auth_decorator(f):
        @wraps(f)
        def __auth_decorator(*args, **kwargs):
            try:
                token = request.headers.get('authorization')
                if(token == None):
                    LOGS_CONFIG.logError("Not authorized for operation. Access denied", logInfo, 401)
                    return jsonify({'error': "Not authorized for operation. Access denied"}), 401
                token = token.replace("Bearer ", "")
                tokenDecoded = jwt.decode(token, APP_CONFIG.JWT_SECRET, algorithms=["HS256"])
                users = ADMIN_CONTROLLER.USERS_MODEL
                try:
                    user = users.users[tokenDecoded["uuid"]]
                except:
                    LOGS_CONFIG.logError("Not authorized for operation. Access denied", logInfo, 401)
                    return jsonify({'error': "Not authorized for operation. Access denied"}), 401
                try:
                    tokenIndex = user.tokens.index(token)
                except:
                    LOGS_CONFIG.logError("Not authorized for operation. Access denied", logInfo, 401)
                    return jsonify({'error': "Not authorized for operation. Access denied"}), 401
                if(datetime.now() > datetime.strptime(tokenDecoded["expiration"], "%Y-%m-%d %H:%M:%S.%f")):
                    del user.tokens[tokenIndex]
                    LOGS_CONFIG.logError("Not authorized for operation. Access denied", logInfo, 401)
                    return jsonify({'error': "Not authorized for operation. Access denied"}), 401
                result = f(*args, **kwargs)
                return result
            except Exception as exc:
                LOGS_CONFIG.logError("Admin authorization has failed. Access denied", logInfo, error=exc)
                return jsonify({'error': "Admin authorization has failed. Access denied"}), 500

        return __auth_decorator
    return _auth_decorator
"""
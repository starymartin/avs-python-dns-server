from flask import jsonify, request
import sources.models.dns.dnsServer_model as DNS_SERVER_MODEL
import configs.app_config as APP_CONFIG
import configs.logger_config as LOGS_CONFIG
import threading
import socket
import copy

logInfo = {"label": "[DNS CONTROLLER]"}

DNS_MESSAGE_MODEL = DNS_SERVER_MODEL.DNS_MESSAGE_MODEL
DNS_SERVER = DNS_SERVER_MODEL.DNS_Server()
DNS_SERVER.seedZones()

SOCK = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
DNS_SERVER_WORKING = True
try:
    SOCK.bind((APP_CONFIG.DNS_IP_ADDRESS, APP_CONFIG.DNS_PORT)) 
except Exception as exc:
    SOCK.close()
    LOGS_CONFIG.logError("Start DNS server failed", logInfo, 6, exc)
    DNS_SERVER_WORKING = False


""" params: None
        Function used for execution target for threading
    return None """
def DNS_RECEIVER():
    while(DNS_SERVER_WORKING):
        LOGS_CONFIG.logError("WAITING TO RECEIVE DNS QUERY", logInfo, -1)
        received = SOCK.recvfrom(512)
        LOGS_CONFIG.logError("DNS QUERY RECEIVED, PROCESSING...", logInfo, -1)
        if(DNS_SERVER_WORKING):
            (dnsRequestData, dnsRequestAddress) = received
            dnsRequestHeader = DNS_MESSAGE_MODEL.DNS_Header.parseHeader(dnsRequestData)
            dnsRequestHeader.flags1.QR = 1
            dnsRequestHeader.ANCOUNT = 0
            dnsRequestHeader.NSCOUNT = 0
            dnsRequestHeader.ARCOUNT = 0

            dnsRequestQueries = DNS_MESSAGE_MODEL.DNS_Queries.parseQueries(dnsRequestData, dnsRequestHeader.QDCOUNT)

            dnsResponseAnswerRRs = DNS_MESSAGE_MODEL.DNS_RRs()
            dnsResponseAuthorityRRs = DNS_MESSAGE_MODEL.DNS_RRs()
            dnsResponseAdditionalRRs = DNS_MESSAGE_MODEL.DNS_RRs()

            for query in dnsRequestQueries.queries:
                domainName = ".".join(query.domainName)
                zone = DNS_SERVER.findZone(domainName)
                dnsSearched = DNS_SERVER.searchByCredetials((zone, query.QTYPE, domainName, query.QNAME))
                if(len(dnsSearched["answerRRs"]) == 0 and len(dnsSearched["authorityRRs"]) == 0 and len(dnsSearched["additionalRRs"]) == 0):
                    dnsRequestHeader.flags2.RCODE = 3
                dnsResponseAnswerRRs.RRs.extend(dnsSearched["answerRRs"])
                dnsResponseAuthorityRRs.RRs.extend(dnsSearched["authorityRRs"])
                dnsResponseAdditionalRRs.RRs.extend(dnsSearched["additionalRRs"])

            dnsRequestHeader.ANCOUNT = len(dnsResponseAnswerRRs.RRs)
            dnsRequestHeader.NSCOUNT = len(dnsResponseAuthorityRRs.RRs)
            dnsRequestHeader.ARCOUNT = len(dnsResponseAdditionalRRs.RRs)
            dnsRequestHeaderBytes = dnsRequestHeader.getHeader()
            dnsRequestQueriesBytes = dnsRequestQueries.getQueries()
            dnsResponseAnswerRRsBytes = dnsResponseAnswerRRs.getRRs()
            dnsResponseAuthorityRRsBytes = dnsResponseAuthorityRRs.getRRs()
            dnsResponseAdditionalRRsBytes = dnsResponseAdditionalRRs.getRRs()

            dnsResponse = dnsRequestHeaderBytes + dnsRequestQueriesBytes + dnsResponseAnswerRRsBytes + dnsResponseAuthorityRRsBytes + dnsResponseAdditionalRRsBytes
            
            SOCK.sendto(dnsResponse, (dnsRequestAddress[0], dnsRequestAddress[1]))

THREADS = list()
for i in range(APP_CONFIG.DNS_AMOUNT_OF_THREADS):
    thread = threading.Thread(target=DNS_RECEIVER)
    THREADS.append(thread)
    thread.start()
    LOGS_CONFIG.logError("THREAD CREATED", logInfo, -1)


""" CONTROLLER FUNCTION of 'PATCH: localhost:<port>/api/dns/start' 
    MORE INFO IN SWAGGER """
def start(policy):
    try:
        global THREADS
        global DNS_SERVER_WORKING
        global SOCK
        DNS_SERVER_WORKING = True
        if(SOCK._closed):
            SOCK = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            try:
                SOCK.bind((APP_CONFIG.DNS_IP_ADDRESS, APP_CONFIG.DNS_PORT)) 
            except Exception as exc:
                SOCK.close()
                LOGS_CONFIG.logError("Start DNS server failed", logInfo, 6, exc)
                DNS_SERVER_WORKING = False
                return jsonify({"error": str(exc)}), 409
        for i in range(APP_CONFIG.DNS_AMOUNT_OF_THREADS):
            if(THREADS[i].is_alive()):
                LOGS_CONFIG.logError("DNS server is already running", logInfo, 409)
                return jsonify({"error": "DNS server is already running"}), 409
            else:
                THREADS[i] = threading.Thread(target=DNS_RECEIVER)
                THREADS[i].start() 
                LOGS_CONFIG.logError("THREAD VYTVORENY", logInfo, -1)           
        return jsonify({"successful": True}), 200
    except Exception as exc:
        LOGS_CONFIG.logError("Start DNS server failed", logInfo, error=exc)
        return jsonify({"error": "Start DNS server failed"}), 500


""" CONTROLLER FUNCTION of 'PATCH: localhost:<port>/api/dns/stop' 
    MORE INFO IN SWAGGER """
def stop(policy):
    try:
        global DNS_SERVER_WORKING
        global SOCK
        DNS_SERVER_WORKING = False
        for thread in THREADS:
            if(not thread.is_alive()):
                LOGS_CONFIG.logError("DNS server is already stopped", logInfo, 409)
                return jsonify({"error": "DNS server is already stopped"}), 409
        try:
            SOCK.shutdown(socket.SHUT_RDWR) 
        except:
            pass
        for thread in THREADS:
            thread.join()
            LOGS_CONFIG.logError("THREAD UKONCENY", logInfo, -1)
        if(not SOCK._closed):
            SOCK.close()
        return jsonify({"successful": True}), 200
    except Exception as exc:
        LOGS_CONFIG.logError("Stop DNS server failed", logInfo, error=exc)
        return jsonify({"error": "Stop DNS server failed"}), 500


""" CONTROLLER FUNCTION of 'GET: localhost:<port>/api/dns/get-zones' 
    MORE INFO IN SWAGGER """
def getZones(policy):
    try:
        return jsonify(list(DNS_SERVER.zones.keys())), 200
    except Exception as exc:
        LOGS_CONFIG.logError("Get DNS zones failed", logInfo, error=exc)
        return jsonify({"error": "Get DNS zones failed"}), 500


""" CONTROLLER FUNCTION of 'GET: localhost:<port>/api/dns/get-zone/<string:zoneDomainName>' 
    MORE INFO IN SWAGGER """
def getZone(policy, zoneDomainName):
    try:
        zone = DNS_SERVER.getClearedZone(zoneString=zoneDomainName)
        if(zone == None):
            LOGS_CONFIG.logError("Zone NOT FOUND", logInfo, 404)
            return jsonify("Zone NOT FOUND"), 404
        return jsonify(zone), 200
    except Exception as exc:
        LOGS_CONFIG.logError("Get DNS zone failed", logInfo, error=exc)
        return jsonify({"error": "Get DNS zone failed"}), 500


""" CONTROLLER FUNCTION of 'GET: localhost:<port>/api/dns/get-record/<string:recordType>/<string:domainName>' 
    MORE INFO IN SWAGGER """
def getRecord(recordType, domainName):
    try:
        response = DNS_SERVER.searchByCredetialsHTTP(recordType, domainName)
        return jsonify(response), 200
    except Exception as exc:
        LOGS_CONFIG.logError("Get DNS record failed", logInfo, error=exc)
        return jsonify({"error": "Get DNS record failed"}), 500


""" CONTROLLER FUNCTION of 'PUT: localhost:<port>/api/dns/add-zone' 
    MORE INFO IN SWAGGER """
def addZone(policy):
    try:
        data = request.get_json()
        if("origin" not in data or "ttl" not in data or "soa" not in data or "mname" not in data["soa"] or "rname" not in data["soa"] or "refresh" not in data["soa"] or "serial" not in data["soa"] or "retry" not in data["soa"] or "expire" not in data["soa"] or "minimum" not in data["soa"]):
            LOGS_CONFIG.logError("Missing requires attributes in body", logInfo, 400)
            return jsonify({"error": "Missing requires attributes in body"}), 400
        if(type(data["origin"]) is not str or type(data["ttl"]) is not int or type(data["soa"]) is not dict or type(data["soa"]["mname"]) is not str or type(data["soa"]["rname"]) is not str or type(data["soa"]["serial"]) is not str or type(data["soa"]["refresh"]) is not int or type(data["soa"]["retry"]) is not int or type(data["soa"]["expire"]) is not int or type(data["soa"]["minimum"]) is not int ):
            LOGS_CONFIG.logError("Wrong type of attribute in body", logInfo, 400)
            return jsonify({"error": "Wrong type of attribute in body"}), 400
        if(DNS_SERVER.zones.get(data["origin"]) != None):
            LOGS_CONFIG.logError("Zone already exists", logInfo, 409)
            return jsonify({"error": "Zone already exists"}), 409
        addedZone = DNS_SERVER.addZone(data["origin"], data["ttl"], data["soa"])
        return jsonify(addedZone), 201
    except Exception as exc:
        LOGS_CONFIG.logError("Add DNS zone failed", logInfo, error=exc)
        return jsonify({"error": "Add DNS zone failed"}), 500


""" CONTROLLER FUNCTION of 'DELETE: localhost:<port>/api/dns/delete-zone/<string:zoneDomainName>' 
    MORE INFO IN SWAGGER """
def deleteZone(policy, zoneDomainName):
    try:
        zone = DNS_SERVER.zones.get(zoneDomainName) 
        if(zone == None):
            LOGS_CONFIG.logError("Zone NOT FOUND", logInfo, 404)
            return jsonify({"error": "Zone NOT FOUND"}), 404
        zone = DNS_SERVER.getClearedZone(zone=DNS_SERVER.zones.pop(zoneDomainName, None))
        return jsonify(zone), 200
    except Exception as exc:
        LOGS_CONFIG.logError("Delete DNS zone failed", logInfo, error=exc)
        return jsonify({"error": "Delete DNS zone failed"}), 500


""" CONTROLLER FUNCTION of 'PUT: localhost:<port>/api/dns/add-record/<string:recordType>/<string:zoneDomainName>' 
    MORE INFO IN SWAGGER """
def addRecord(policy, recordType, zoneDomainName):
    try:
        zone = DNS_SERVER.zones.get(zoneDomainName)
        if(zone == None):
            LOGS_CONFIG.logError("Zone NOT FOUND", logInfo, 404)
            return jsonify({"error": "Zone NOT FOUND"}), 404
        data = request.get_json()
        response = None
        if(recordType == "NS"):
            if("host" not in data):
                LOGS_CONFIG.logError("Missing requires attributes in body", logInfo, 400)
                return jsonify({"error": "Missing requires attributes in body"}), 400
            if(type(data["host"]) is not str):
                LOGS_CONFIG.logError("Wrong type of attribute in body", logInfo, 400)
                return jsonify({"error": "Wrong type of attribute in body"}), 400
            if(zone.get(recordType + "_RECORDS") == None):
                zone[recordType + "_RECORDS"] = {}
            if(zone[recordType + "_RECORDS"].get(data["host"]) != None):
                LOGS_CONFIG.logError("Record already exists", logInfo, 409)
                return jsonify({"error": "Record already exists"}), 409
            response = {
                "host": data["host"]
            }
            zone["NS_RECORDS"][data["host"]] = copy.deepcopy(response)
            zone["NS_RECORDS"][data["host"]]["hostBytes"] = DNS_MESSAGE_MODEL.formateDomainName(data["host"])
        elif(recordType == "A" or recordType == "AAAA" or recordType == "CNAME"):
            if("name" not in data or "ttl" not in data or "value" not in data):
                LOGS_CONFIG.logError("Missing requires attributes in body", logInfo, 400)
                return jsonify({"error": "Missing requires attributes in body"}), 400
            if(type(data["name"]) is not str or type(data["ttl"]) is not int or type(data["value"]) is not str):
                LOGS_CONFIG.logError("Wrong type of attribute in body", logInfo, 400)
                return jsonify({"error": "Wrong type of attribute in body"}), 400
            if(zone.get(recordType + "_RECORDS") == None):
                zone[recordType + "_RECORDS"] = {}
            if(zone[recordType + "_RECORDS"].get(data["name"]) != None):
                LOGS_CONFIG.logError("Record already exists", logInfo, 409)
                return jsonify({"error": "Record already exists"}), 409
            response = {
                "ttl": data["ttl"],
                "value": data["value"]
            }
            zone[recordType + "_RECORDS"][data["name"]] = copy.deepcopy(response)
            response["name"] = data["name"]
            zone[recordType + "_RECORDS"][data["name"]]["nameBytes"] = DNS_MESSAGE_MODEL.formateDomainName(data["name"])
            zone[recordType + "_RECORDS"][data["name"]]["valueBytes"] = DNS_MESSAGE_MODEL.formateDomainName(data["value"])
        else:
            LOGS_CONFIG.logError("Not supported record type", logInfo, 400)
            return jsonify({"error": "Not supported record type"}), 400
        return jsonify(response), 201
    except Exception as exc:
        LOGS_CONFIG.logError("Add DNS record failed", logInfo, error=exc)
        return jsonify({"error": "Add DNS record failed"}), 500


""" CONTROLLER FUNCTION of 'DELETE: localhost:<port>/api/dns/delete-record/<string:recordType>/<string:domainName>' 
    MORE INFO IN SWAGGER """
def deleteRecord(policy, recordType, domainName):
    try:
        zoneName = DNS_SERVER.findZone(domainName)
        if(zoneName == None):
            LOGS_CONFIG.logError("Zone NOT FOUND", logInfo, 404)
            return jsonify({"error": "Zone NOT FOUND"}), 404
        zone = DNS_SERVER.zones.get(zoneName) 
        zoneRecordType = zone.get(recordType + "_RECORDS")
        if(zoneRecordType == None):
            LOGS_CONFIG.logError("Zone has not defined records of requested type", logInfo, 404)
            return jsonify({"error": "Zone has not defined records of requested type"}), 404
        record = zoneRecordType.get(domainName)
        if(record == None):
            LOGS_CONFIG.logError("Zone has not defined requested record", logInfo, 404)
            return jsonify({"error": "Zone has not defined requested record"}), 404
        record = zoneRecordType.pop(domainName)
        response = {}
        if(recordType == "NS"):
            response = {
                "host": record["host"]
            }
        elif(recordType == "A" or recordType == "AAAA" or recordType == "CNAME"):
            response = {
                "name": domainName,
                "ttl": record["ttl"],
                "value": record["value"]
            }
        else:
            LOGS_CONFIG.logError("Not supported record type", logInfo, 400)
            return jsonify({"error": "Not supported record type"}), 400
        return jsonify(response), 200
    except Exception as exc:
        LOGS_CONFIG.logError("Delete DNS record failed", logInfo, error=exc)
        return jsonify({"error": "Delete DNS record failed"}), 500
import base64
from flask import jsonify, request
import configs.logger_config as LOGS_CONFIG
import sources.models.user.user_model as USER
import uuid
import bcrypt

logInfo = {"label": "[ADMIN CONTROLLER]"}
USERS_MODEL = USER.Users()
USERS_MODEL.seedUsers()


""" CONTROLLER FUNCTION of 'POST: localhost:<port>/api/admin/login' 
    MORE INFO IN SWAGGER """
def login():
    try:
        auth = request.headers.get('authorization')
        if(auth == None):
            LOGS_CONFIG.logError("Admin is not authorized. Access denied", logInfo, 401)
            return jsonify({"error": "Admin is not authorized. Access denied"}), 401

        auth = auth.replace("Basic ", "").encode('ascii')
        try:
            auth = base64.b64decode(auth)
            auth = auth.decode('ascii').split(":")
            username = auth[0]
            password = auth[1]
        except Exception as exc:
            LOGS_CONFIG.logError("Admin is not authorized. Access denied", logInfo, 401)
            return jsonify({"error": "Admin is not authorized. Access denied"}), 401
        user = USERS_MODEL.getUserByUsername(username)
        if(user == None):
            LOGS_CONFIG.logError("Admin is not authorized. Access denied", logInfo, 401)
            return jsonify({"error": "Admin is not authorized. Access denied"}), 401

        loginResult = user.login(username, password)
        if(loginResult["successful"] == False):
            LOGS_CONFIG.logError(loginResult["error"], logInfo, 401)
            return jsonify({"error": loginResult["error"]}), 401
        return jsonify({"access_token" : loginResult["token"], "token_type" : "bearer"}), 200
    except Exception as exc:
        LOGS_CONFIG.logError("Admin login failed", logInfo, error=exc)
        return jsonify({"error": "Admin login failed"}), 500


""" CONTROLLER FUNCTION of 'POST: localhost:<port>/api/admin/logout' 
    MORE INFO IN SWAGGER """
def logout(policy):
    try:
        user = policy["user"]
        del user.tokens[policy["tokenIndex"]]
        return jsonify({"successful": True}), 200
    except Exception as exc:
        LOGS_CONFIG.logError("Admin logout failed", logInfo, error=exc)
        return jsonify({"error": "Admin logout failed"}), 500

""" CONTROLLER FUNCTION of 'GET: localhost:<port>/api/admin/returnUserData' 
    MORE INFO IN SWAGGER """
def returnUserData():
    try:
        data = request.get_json()
        if("username" not in data or "password" not in data):
            LOGS_CONFIG.logError("Missing requires attributes in body", logInfo, 400)
            return jsonify({"error": "Missing requires attributes in body"}), 400
        if(type(data["username"]) is not str or type(data["password"]) is not str):
            LOGS_CONFIG.logError("Wrong type of attribute in body", logInfo, 400)
            return jsonify({"error": "Wrong type of attribute in body"}), 400
        result = {}
        result["uuid"] = uuid.uuid4()
        result["passwordHash"] = (bcrypt.hashpw(data["password"].encode('ascii'), bcrypt.gensalt())).decode()
        result["username"] = data["username"]
        print(result)
        return jsonify(result), 200
    except Exception as exc:
        LOGS_CONFIG.logError("Return user data failed", logInfo, error=exc)
        return jsonify({"error": "Return user data failed"}), 500
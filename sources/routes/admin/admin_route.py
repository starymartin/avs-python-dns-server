from flask import Blueprint, jsonify

import sources.controllers.admin.admin_controller as CONTROLLER
import sources.middlewares.policy_middleware as POLICY

admin = Blueprint('admin', __name__)


""" PUBLIC
    POST: localhost:<port>/api/admin/login
    EMPTY BODY 
    REQUIRED HEADERS: {authorization, format:'Basic {base64}'} """
@admin.route('/login', methods=['POST'])
def login():
    """
    swagger_from_file: admin/docs/login.yaml
    """
    return CONTROLLER.login()


""" ADMIN-ONLY
    POST: localhost:<port>/api/admin/logout
    EMPTY BODY """
@admin.route('/logout', methods=['POST'])
def logout():
    """
    swagger_from_file: admin/docs/logout.yaml
    """
    policy = POLICY.auth()
    if(policy["success"] == False):
        return jsonify({"error": policy["error"]}), policy["errorCode"]
    return CONTROLLER.logout(policy)

""" PUBLIC
    GET: localhost:<port>/api/admin/returnUserData
    REQUIRED BODY """
@admin.route('/returnUserData', methods=['GET'])
def returnUserData():
    """
    swagger_from_file: admin/docs/returnUserData.yaml
    """
    return CONTROLLER.returnUserData()

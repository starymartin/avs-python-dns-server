from flask import Blueprint, jsonify

import sources.controllers.dns.dns_controller as CONTROLLER
import sources.middlewares.policy_middleware as POLICY

dns = Blueprint('dns', __name__)


""" ADMIN-ONLY
    PATCH: localhost:<port>/api/dns/start 
    EMPTY BODY """
@dns.route('/start', methods=['PATCH'])
def start():
    """
    swagger_from_file: dns/docs/start.yaml
    """
    policy = POLICY.auth()
    if(policy["success"] == False):
        return jsonify({"error": policy["error"]}), policy["errorCode"]
    return CONTROLLER.start(policy)


""" ADMIN-ONLY
    PATCH: localhost:<port>/api/dns/stop
    EMPTY BODY """
@dns.route('/stop', methods=['PATCH'])
def stop():
    """
    swagger_from_file: dns/docs/stop.yaml
    """
    policy = POLICY.auth()
    if(policy["success"] == False):
        return jsonify({"error": policy["error"]}), policy["errorCode"]
    return CONTROLLER.stop(policy)


""" ADMIN-ONLY
    GET: localhost:<port>/api/dns/get-zones
    EMPTY BODY """
@dns.route('/get-zones', methods=['GET'])
def getZones():
    """
    swagger_from_file: dns/docs/getZones.yaml
    """
    policy = POLICY.auth() 
    if(policy["success"] == False):
        return jsonify({"error": policy["error"]}), policy["errorCode"]
    return CONTROLLER.getZones(policy)


""" ADMIN-ONLY
    GET: localhost:<port>/api/dns/get-zone/<string:zoneDomainName>
    EMPTY BODY """
@dns.route('/get-zone/<string:zoneDomainName>', methods=['GET'])
def getZone(zoneDomainName):
    """
    swagger_from_file: dns/docs/getZone.yaml
    """
    policy = POLICY.auth() 
    if(policy["success"] == False):
        return jsonify({"error": policy["error"]}), policy["errorCode"]
    return CONTROLLER.getZone(policy, zoneDomainName)


""" PUBLIC
    GET: localhost:<port>/api/dns/get-record/<string:recordType>/<string:domainName>
    EMPTY BODY """
@dns.route('/get-record/<string:recordType>/<string:domainName>', methods=['GET'])
def getRecord(recordType, domainName):
    """
    swagger_from_file: dns/docs/getRecord.yaml
    """
    return CONTROLLER.getRecord(recordType, domainName)


""" ADMIN-ONLY
    PUT: localhost:<port>/api/dns/add-zone
    REQUIRED BODY """
@dns.route('/add-zone', methods=['PUT'])
def addZone():
    """
    swagger_from_file: dns/docs/addZone.yaml
    """
    policy = POLICY.auth() 
    if(policy["success"] == False):
        return jsonify({"error": policy["error"]}), policy["errorCode"]
    return CONTROLLER.addZone(policy)


""" ADMIN-ONLY
    PUT: localhost:<port>/api/dns/delete-zone/<string:zoneDomainName>
    EMPTY BODY """
@dns.route('/delete-zone/<string:zoneDomainName>', methods=['DELETE'])
def deleteZone(zoneDomainName):
    """
    swagger_from_file: dns/docs/deleteZone.yaml
    """
    policy = POLICY.auth() 
    if(policy["success"] == False):
        return jsonify({"error": policy["error"]}), policy["errorCode"]
    return CONTROLLER.deleteZone(policy, zoneDomainName)


""" ADMIN-ONLY
    PUT: localhost:<port>/api/dns/add-record/<string:recordType>/<string:zoneDomainName>
    REQUIRED BODY """
@dns.route('/add-record/<string:recordType>/<string:zoneDomainName>', methods=['PUT'])
def addRecord(recordType, zoneDomainName):
    """
    swagger_from_file: dns/docs/addRecord.yaml
    """
    policy = POLICY.auth() 
    if(policy["success"] == False):
        return jsonify({"error": policy["error"]}), policy["errorCode"]
    return CONTROLLER.addRecord(policy, recordType, zoneDomainName)


""" ADMIN-ONLY
    PUT: localhost:<port>/api/dns/delete-record/<string:recordType>/<string:domainName>
    EMPTY BODY """
@dns.route('/delete-record/<string:recordType>/<string:domainName>', methods=['DELETE'])
def deleteRecord(recordType, domainName):
    """
    swagger_from_file: dns/docs/deleteRecord.yaml
    """
    policy = POLICY.auth() 
    if(policy["success"] == False):
        return jsonify({"error": policy["error"]}), policy["errorCode"]
    return CONTROLLER.deleteRecord(policy, recordType, domainName)
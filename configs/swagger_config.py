import os
import json
from flask_swagger_ui import get_swaggerui_blueprint
from flask import jsonify
from flask_swagger import swagger
import configs.app_config as APP_CONFIG

SWAGGER_URL = os.environ.get('SWAGGER_URL')

SWAGGER_UI_FLASK_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    APP_CONFIG.APP_LINK + SWAGGER_URL + ".json",
    config={
        "app_name": "AvS DNS Server",
        "explorer": "false"
    }
)

def SWAGGER_JSON(app):
    base_path = os.path.join(APP_CONFIG.APP_ROOT, 'sources', 'routes')
    swag = swagger(app, from_file_keyword = "swagger_from_file", base_path = base_path)
    swag['info']['version'] = "1.0"
    swag['info']['title'] = "AvS DNS Server"
    swag['info']['description'] = "REST API for DNS server"
    swag['securityDefinitions'] = {
        "oauth2": {
            "type": "oauth2",
            "tokenUrl": APP_CONFIG.APP_LINK + APP_CONFIG.APP_ROUTE_PREFIX + "/admin/login",
            "flow": "application"
        }
    }
    swag['components'] = { 
        "responses": {
            "unauthorized": {
                "description": "Not authorized for operation. Access denied / Admin authorization has failed. Access denied",
                "schema": {
                    "type": "object",
                    "properties": {
                        "error": {
                            "type": "string",
                            "example": {
                                "<oneOf>": [
                                    "Not authorized for operation. Access denied",
                                    "Admin authorization has failed. Access denied"
                                ]
                            }
                        }
                    }
                }
            }
        },
        "schemas": {
            "resourceRecord": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "DNS domain name, which is in question or in relation with returned data"
                    },
                    "TTL": {
                        "type": "integer",
                        "description": "Resource record TTL"
                    }, 
                    "data": {
                        "type": "string",
                        "description": "Data DNS server returns"
                    },
                    "type": {
                        "type": "integer",
                        "description": "Number representation of DNS question type"
                    }
                }
            }
        }
    }
    return jsonify(swag)
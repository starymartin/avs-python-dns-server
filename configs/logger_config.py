import os
import logging
import sys
import traceback
from logging.handlers import TimedRotatingFileHandler

#=========DISABLE FLASK LOGS=========#

flaskLog = logging.getLogger('werkzeug')
flaskLog.disabled = True

#=========LOGGER COLORS=========#

class CustomFormatter(logging.Formatter):
    cyan = "\x1b[36;21m"
    green = "\x1b[32;21m"
    yellow = "\x1b[33;21m"
    purple = "\x1b[35;21m"
    red = "\x1b[31;21m"
    reset = "\x1b[0m"
    format = "%(asctime)s [%(levelname).4s] - %(message)s"

    FORMATS = {
        logging.DEBUG: cyan + format + reset,
        logging.INFO: green + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: purple + format + reset,
        logging.CRITICAL: red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

#=========LOGGER SETUP=========#

switch = {
    "CRITICAL": logging.CRITICAL,
    "ERROR": logging.ERROR,
    "WARNING": logging.WARNING,
    "INFO": logging.INFO,
    "DEBUG": logging.DEBUG,
    "NOTSET": logging.NOTSET,
}
LOG_LEVEL = switch.get(os.environ.get('LOG_LEVEL'), "Undefined logging level")
LOG_BACKUP_COUNT = os.environ.get('LOG_BACKUP_COUNT')
LOG_WHEN = os.environ.get('LOG_WHEN')
LOG_INTERVAL = os.environ.get('LOG_INTERVAL')

logFileHandler = TimedRotatingFileHandler("logs/application.log", when=LOG_WHEN, interval=int(LOG_INTERVAL), backupCount=int(LOG_BACKUP_COUNT))
logFileFormatter = logging.Formatter('%(asctime)s [%(levelname).4s] - %(message)s')
logFileHandler.setFormatter( logFileFormatter )
logFileHandler.setLevel( LOG_LEVEL )

logConsoleHandler = logging.StreamHandler(sys.stdout)
logConsoleHandler.setFormatter( CustomFormatter() )
logConsoleHandler.setLevel( LOG_LEVEL )

logging.basicConfig(level=LOG_LEVEL, handlers=[logFileHandler, logConsoleHandler])

#=========LOGGER FUNCTIONS=========#

def logError(message, logInfo, errorCode = 500, error = None):
    errorCategory = errorCode if errorCode == -1 or errorCode == 0 or errorCode == 6 else errorCode // 100
    errorMessage = logInfo["label"]
    if(errorCategory != errorCode):
        errorMessage += " [" + str(errorCode) + "]"
    errorMessage += " " + message
    if(error != None):
        errorMessage += "\n\n" + traceback.format_exc()
    switchLocal = {
        -1: logging.info,
        0: logging.debug,
        4: logging.warn,
        5: logging.error,
        6: logging.critical
    }
    switchLocal.get(errorCategory, "Undefined error category")(errorMessage)

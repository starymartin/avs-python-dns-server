import os

APP_SCHEMA = os.environ.get('APP_SCHEMA')
APP_HOST = os.environ.get('APP_HOST')
APP_PORT = os.environ.get('APP_PORT')
APP_ROUTE_PREFIX = os.environ.get('APP_ROUTE_PREFIX')
APP_LINK = str(APP_SCHEMA) + str("://") + str(APP_HOST) + str(":") + str(APP_PORT)
APP_MODE = os.environ.get('APP_MODE')
USER_MAX_TOKENS = int(os.environ.get('USER_MAX_TOKENS'))
USER_TOKEN_EXPIRATION = int(os.environ.get('USER_TOKEN_EXPIRATION'))
JWT_SECRET = os.environ.get('JWT_SECRET')
APP_ROOT = ""
DNS_AMOUNT_OF_THREADS = int(os.environ.get('DNS_AMOUNT_OF_THREADS'))
DNS_IP_ADDRESS = os.environ.get('DNS_IP_ADDRESS')
DNS_PORT = int(os.environ.get('DNS_PORT'))
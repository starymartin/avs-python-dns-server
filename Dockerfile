# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster
RUN apt-get update
RUN apt-get install -y git net-tools htop

WORKDIR /app

COPY . .

RUN pip3 install -r requirements.txt

RUN chmod a+x main.py

CMD [ "python3", "main.py"]
